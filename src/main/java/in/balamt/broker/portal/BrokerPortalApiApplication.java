package in.balamt.broker.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
@ComponentScan(basePackages = {"in.balamt"})
public class BrokerPortalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrokerPortalApiApplication.class, args);
	}

}
