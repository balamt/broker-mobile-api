package in.balamt.broker.portal.ocr;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.leptonica.global.lept;
import org.bytedeco.tesseract.TessBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.FileStorageProperties;
import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.model.Employment;
import in.balamt.broker.portal.ocr.exception.FileStorageException;
import in.balamt.broker.portal.ocr.exception.MyFileNotFoundException;
import in.balamt.broker.portal.ocr.util.CustomerNumberGen;
import in.balamt.broker.portal.ocr.util.LoanNumberGen;
import in.balamt.broker.portal.ocr.util.OcrUtil;
import in.balamt.broker.portal.repository.DealRepository;

@Service
public class OcrProcessorServiceImpl implements OcrProcessorService {

	public static final Logger LOGGER = Logger.getLogger(OcrProcessorServiceImpl.class.getName());
	private final Path fileStorageLocation;

	@Autowired
	DealRepository dealRepository;

	@Autowired
	public OcrProcessorServiceImpl(FileStorageProperties fileStorageProperties) throws FileStorageException {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	@Override
	public Deal getApplicationContent(MultipartFile file) throws FileStorageException {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// TODO: This has to be eliminated, when we have the authorized user or role
			// based authorization implemented.
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			/**
			 * Initializing the OCR
			 */

			BytePointer outText;
			TessBaseAPI api = new TessBaseAPI();

			String executionPath = System.getProperty("user.dir");
			if (api.Init(executionPath, "eng") != 0) {
				System.err.println("Could not initialize tesseract.");
				System.exit(1);
			}

			// Open input image with leptonica library
			PIX image = lept.pixRead(targetLocation.toString());
			api.SetImage(image);
			// Get OCR result
			outText = api.GetUTF8Text();
			Customer customer = OcrUtil.MapCustomerData(outText.getString());
			Employment employment = OcrUtil.MapEmploymentData(outText.getString());
			customer.setEmployment(employment);
			// System.out.println("OCR output:\n" + customer.toString());

			// Destroy used object and release memory
			api.End();
			outText.deallocate();
			lept.pixDestroy(image);

			/**
			 * End of Processing the image
			 */
			
			Deal deal = new Deal();
			CustomerNumberGen cusGen = new CustomerNumberGen();
			LoanNumberGen lonGen = new LoanNumberGen();
			
			customer.setCif(cusGen.generateCIF());
			deal.setCustomer(customer);
			deal.setDealId(lonGen.generateDealNumber());
			dealRepository.save(deal);

			return deal;
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	public Resource loadFileAsResource(String fileName) throws MyFileNotFoundException {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new MyFileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new MyFileNotFoundException("File not found " + fileName, ex);
		}
	}

	@Override
	public Deal getDeal(String dealNumber) throws IOException {
		Deal deal = new Deal();
		deal.setDealId(Long.parseLong(dealNumber));
		return dealRepository.getDeal(deal);
	}

}
