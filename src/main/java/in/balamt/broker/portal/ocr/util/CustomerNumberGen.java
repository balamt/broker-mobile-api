package in.balamt.broker.portal.ocr.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.util.DefaultPropertiesPersister;

public class CustomerNumberGen {
	private static final String CIF = "CIF";
	private static final String CIF_PROPERTIES_FILE_NAME = "cif.properties";
	public static final Logger LOGGER = Logger.getLogger(CustomerNumberGen.class.getName());

	public Long generateCIF() {
		Long newCIF = 0l;
		try {
			newCIF = getLastCIFFromPropertyFile() + 1;
			setLastCIFFromPropertyFile(newCIF);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
		return newCIF;
	}

	private Long getLastCIFFromPropertyFile() throws IOException {
		Properties pro = new Properties();
		InputStream is = getClass().getClassLoader().getResourceAsStream(CIF_PROPERTIES_FILE_NAME);
		pro.load(is);
		is.close();
		return Long.parseLong(pro.getProperty(CIF, "0"));
	}

	private void setLastCIFFromPropertyFile(Long cif) throws IOException {
		Properties pro = new Properties();
		pro.setProperty(CIF, cif.toString());

		File outFile = new File(getClass().getClassLoader().getResource(CIF_PROPERTIES_FILE_NAME).getFile());
		OutputStream out = new FileOutputStream(outFile);
		DefaultPropertiesPersister p = new DefaultPropertiesPersister();
		p.store(pro, out, "CIF Sequence");
		out.close();

	}
}
