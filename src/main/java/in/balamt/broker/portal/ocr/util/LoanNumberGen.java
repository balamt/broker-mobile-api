package in.balamt.broker.portal.ocr.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.util.DefaultPropertiesPersister;

public class LoanNumberGen {
	private static final String DEAL = "DEAL";
	private static final String DEAL_PROPERTIES_FILE_NAME = "deal.properties";
	public static final Logger LOGGER = Logger.getLogger(LoanNumberGen.class.getName());

	public Long generateDealNumber() {
		Long newDealNumber = 0l;
		try {
			newDealNumber = getLastCIFFromPropertyFile() + 1;
			setLastDealFromPropertyFile(newDealNumber);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
		return newDealNumber;
	}

	private Long getLastCIFFromPropertyFile() throws IOException {
		Properties pro = new Properties();
		InputStream is = getClass().getClassLoader().getResourceAsStream(DEAL_PROPERTIES_FILE_NAME);
		pro.load(is);
		is.close();
		return Long.parseLong(pro.getProperty(DEAL, "0"));
	}

	private void setLastDealFromPropertyFile(Long dealNum) throws IOException {
		Properties pro = new Properties();
		pro.setProperty(DEAL, dealNum.toString());

		File outFile = new File(getClass().getClassLoader().getResource(DEAL_PROPERTIES_FILE_NAME).getFile());
		OutputStream out = new FileOutputStream(outFile);
		DefaultPropertiesPersister p = new DefaultPropertiesPersister();
		p.store(pro, out, "Deal Sequence");
		out.close();

	}
}
