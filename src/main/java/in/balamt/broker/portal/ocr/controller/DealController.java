package in.balamt.broker.portal.ocr.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.OcrProcessorService;
import in.balamt.broker.portal.ocr.exception.FileStorageException;
import in.balamt.broker.portal.ocr.util.CustomerNumberGen;

@RestController
@RequestMapping(value = "/deal")
public class DealController {

	@Autowired
	OcrProcessorService ocrProcessorService;

	@PostMapping("/new")
	public ResponseEntity<Deal> uploadFile(@RequestParam("file") MultipartFile file) throws FileStorageException {
		Deal deal = ocrProcessorService.getApplicationContent(file);
		return ResponseEntity.ok().body(deal);
	}

	@GetMapping("/{dealNumber}")
	public ResponseEntity<Deal> getDeal(@PathVariable("dealNumber") String dealNumber) throws IOException {
		return ResponseEntity.ok().body(ocrProcessorService.getDeal(dealNumber));
	}

}
