package in.balamt.broker.portal.ocr;

import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.exception.FileStorageException;

@Service
public interface OcrProcessorService {
	
	public Deal getApplicationContent(MultipartFile file) throws FileStorageException;
	public Deal getDeal(String dealNumber) throws IOException;

}
