package in.balamt.broker.portal.ocr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.OcrProcessorService;
import in.balamt.broker.portal.ocr.exception.FileStorageException;

@RestController
public class FileController {
	
	@Autowired
	OcrProcessorService ocrProcessorService;
	
	@PostMapping("/uploadFile")
    public ResponseEntity<Deal> uploadFile(@RequestParam("file") MultipartFile file) throws FileStorageException {
		Deal customer = ocrProcessorService.getApplicationContent(file);
        return ResponseEntity.ok().body(customer);
    }

}
