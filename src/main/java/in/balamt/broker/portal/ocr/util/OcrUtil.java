package in.balamt.broker.portal.ocr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.balamt.broker.portal.model.Customer;
import in.balamt.broker.portal.model.Employment;

public class OcrUtil {

	private static final String NEW_LINE = "\n";
	private static final String EMPTY_STRING = "";
	private static final String $AUD = "$AUD";
	private static final String MM_DD_YYYY = "MM/dd/yyyy";
	private static final String SALARY_NET = "Salary (Net) :";
	private static final String DESIGNATION = "Designation :";
	private static final String STARTED_WORKING_FROM = "Started Working From :";
	private static final String COMPANY_TYPE = "Company Type :";
	private static final String COMPANY_NAME = "Company Name :";
	private static final String ADDRESS = "Address :";
	private static final String MOBILE = "Mobile:";
	private static final String EMAIL = "Email :";
	private static final String LAST_NAME = "Last Name :";
	private static final String FIRST_NAME = "First Name :";
	private static final String WORK = "Work :";

	private OcrUtil() {
	}

	public static Customer MapCustomerData(String rawData) {

		String[] stringArr = rawData.split(NEW_LINE);

		Customer customer = new Customer();
		int counter = 0;

		for (String s : stringArr) {
			s = s.replace("_", EMPTY_STRING);
			if (s.contains(FIRST_NAME)) {
				customer.setFirstname(getData(s, FIRST_NAME));
			}
			if (s.contains(LAST_NAME)) {
				customer.setLastname(getData(s, LAST_NAME));
			}

			if (s.contains(EMAIL)) {
				customer.setEmail(getData(s, EMAIL));
			}

			if (s.contains(MOBILE)) {
				customer.setMobile(getData(s, MOBILE));
			}

			if (s.contains(WORK)) {
				customer.setWork(getData(s, WORK));
			}

			if (s.contains(ADDRESS) && counter == 0) {
				customer.setAddress(getData(s, ADDRESS));
				counter++;
			}
		}

		return customer;
	}

	public static Employment MapEmploymentData(String rawData) {

		String[] stringArr = rawData.split(NEW_LINE);

		Employment employment = new Employment();

		for (String s : stringArr) {
			s = s.replace("_", EMPTY_STRING);
			if (s.contains(COMPANY_NAME)) {
				employment.setCompanyName(getData(s, COMPANY_NAME));
			}
			if (s.contains(COMPANY_TYPE)) {
				employment.setCompanyType(getData(s, COMPANY_TYPE));
			}

			if (s.contains(STARTED_WORKING_FROM)) {
				String doj = getData(s, STARTED_WORKING_FROM);
				if (doj.trim().length() <= 0) {
					employment.setStartedWorkingFrom(null);
				} else {
					Date dateOfJoin;
					try {
						dateOfJoin = new SimpleDateFormat(MM_DD_YYYY).parse(doj);
						employment.setStartedWorkingFrom(dateOfJoin);
					} catch (ParseException e) {
					}
				}
			}

			if (s.contains(DESIGNATION)) {
				employment.setDesignation(getData(s, DESIGNATION));
			}

			if (s.contains(SALARY_NET)) {

				String sal = getData(s, SALARY_NET).trim().replace($AUD, EMPTY_STRING);
				employment.setSalary(sal);
			}

			if (s.contains(ADDRESS)) {
				employment.setAddress(getData(s, ADDRESS));
			}
		}

		return employment;
	}

	private static String getData(String data, String type) {
		return data.replace(type, EMPTY_STRING).trim();
	}

}
