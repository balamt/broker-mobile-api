package in.balamt.broker.portal.ocr.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SerializeAndDeSerialize {

	public static final Logger LOGGER = Logger.getLogger(SerializeAndDeSerialize.class.getName());

	public static void Serialize(Object obj, String fileNameWithPath) throws IOException {

		ObjectOutputStream oos = null;
		FileOutputStream fout = null;

		try {
			System.out.println("FPath " + fileNameWithPath);
			fout = new FileOutputStream(fileNameWithPath, true);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(obj);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.log(Level.SEVERE, ex.getMessage());
		} finally {
			if (oos != null) {
				oos.close();
			}
		}

	}

	public static Object deSerializeData(String fileNameWithPath) throws IOException {
		ObjectInputStream objectinputstream = null;
		Object obj = null;
		try {
			FileInputStream streamIn = new FileInputStream(fileNameWithPath);
			objectinputstream = new ObjectInputStream(streamIn);
			obj = objectinputstream.readObject();
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage());
		} finally {
			if (objectinputstream != null) {
				objectinputstream.close();
			}
		}
		return obj;
	}

}
