package in.balamt.broker.portal.model;

public enum LoanType {
	
	MORTGAGE("MORTGAGE"),
	PERSONAL("PERSONAL"),
	CREDITCARD("CREDITCARD");
	
	private String code;
	
	LoanType(String code){
		this.code = code;
	}
	
	public String getCode() {
		return this.code;
	}

}
