package in.balamt.broker.portal.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

public class Employment extends ResourceSupport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String companyName;
	String companyType;
	Date startedWorkingFrom;
	String designation;
	String salary;
	String address;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public Date getStartedWorkingFrom() {
		return startedWorkingFrom;
	}

	public void setStartedWorkingFrom(Date startedWorkingFrom) {
		this.startedWorkingFrom = startedWorkingFrom;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employment [companyName=" + companyName + ", companyType=" + companyType + ", startedWorkingFrom="
				+ startedWorkingFrom + ", designation=" + designation + ", salary=" + salary + ", address=" + address
				+ "]";
	}

}
