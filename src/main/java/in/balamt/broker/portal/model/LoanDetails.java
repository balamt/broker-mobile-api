package in.balamt.broker.portal.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class LoanDetails extends ResourceSupport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	LoanType loanType;
	double loanAmount;
	float loanTenure;
	float intrest;
	Date createdDate;
	Date fundedDate;

	public LoanType getLoanType() {
		return loanType;
	}
	public void setLoanType(LoanType loanType) {
		this.loanType = loanType;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public float getLoanTenure() {
		return loanTenure;
	}
	public void setLoanTenure(float loanTenure) {
		this.loanTenure = loanTenure;
	}
	public float getIntrest() {
		return intrest;
	}
	public void setIntrest(float intrest) {
		this.intrest = intrest;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getFundedDate() {
		return fundedDate;
	}
	public void setFundedDate(Date fundedDate) {
		this.fundedDate = fundedDate;
	}
	@Override
	public String toString() {
		return "LoanDetails [loanType=" + loanType + ", loanAmount=" + loanAmount
				+ ", loanTenure=" + loanTenure + ", intrest=" + intrest + ", createdDate=" + createdDate
				+ ", fundedDate=" + fundedDate + "]";
	}
	

}
