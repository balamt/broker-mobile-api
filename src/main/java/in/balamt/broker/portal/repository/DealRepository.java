package in.balamt.broker.portal.repository;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Component;

import in.balamt.broker.portal.model.Deal;
import in.balamt.broker.portal.ocr.util.SerializeAndDeSerialize;

@Component
public class DealRepository {

	private final String executionPath = System.getProperty("user.dir");
	private final File dealsFolder = new File(executionPath + "/deals");

	public void save(Deal deal) throws IOException {

		String fileName = deal.getDealId().toString() + ".dat";

		if (!dealsFolder.exists()) {
			dealsFolder.mkdir();
		}
		SerializeAndDeSerialize.Serialize(deal, executionPath + "/deals/" + fileName);
	}

	public Deal getDeal(Deal deal) throws IOException {
		String fileName = deal.getDealId().toString() + ".dat";
		return (Deal) SerializeAndDeSerialize.deSerializeData(executionPath + "/deals/" + fileName);
	}

}
